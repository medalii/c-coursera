#include <iostream>
#include <vector>
#include <string>
using namespace std;

/*******************************************
 * Completez le programme a partir d'ici.
 *******************************************/
// Chaines de caractères à utiliser pour les affichages:
/*

été jeté

d'un

n'est

L'oeuvre

bibliothèque

détruit

*/
#include <algorithm>
#include <memory>

class Auteur {
    private:
        const string nom;
        bool prime;

    public:
        Auteur(const string nom, bool prime=false) : nom(nom), prime(prime) {}
        Auteur(Auteur const &copy) = delete;
        string getNom() const {return nom;}
        bool getPrix() const {return prime;}
};

class Oeuvre {
    private:
        const string titre;
        const Auteur& auteur;
        const string langue;
    public:
        Oeuvre(const string titre,const  Auteur &auteur, const string langue) : titre(titre), auteur(auteur), langue(langue) {}
        ~Oeuvre() {
        cout << "L'oeuvre \""<<titre<<", "<<auteur.getNom()<<", en "<<langue<<"\" n'est plus disponible." << endl;}
        const Auteur& getAuteur() const {return auteur;}
        string getLangue() const {return langue;}
        string getTitre() const {return titre;}
        void affiche() {cout << titre<<", "<<auteur.getNom()<<", en "<< langue << endl;}
};

class Exemplaire
{
    private:
        const Oeuvre  &oeuvre ;
    public:
        Exemplaire(Oeuvre const& o) : oeuvre(o)
        {   cout<< "Nouvel exemplaire de : "<<o.getTitre()<<", "<<o.getAuteur().getNom()<<", en "<<o.getLangue()<<endl; }

        Exemplaire( Exemplaire const&  e) : oeuvre(e.getOeuvre())
{   cout<<"Copie d’un exemplaire de : "<<oeuvre.getTitre()<<", "<<oeuvre.getAuteur().getNom()<<", en "<<oeuvre.getLangue()<<endl;}
        
        Exemplaire(Exemplaire&& ex) : oeuvre(ex.getOeuvre()) {}
         
        ~Exemplaire()
{ cout <<"Un exemplaire de \""<<oeuvre.getTitre()<<", "<<oeuvre.getAuteur().getNom()<<", en "<< oeuvre.getLangue()<<"\" a été jeté !"<< endl;}

        const Oeuvre& getOeuvre() const { return oeuvre ;}

        void affiche() 
{cout << "Exemplaire de : "<< oeuvre.getTitre() <<", "<< oeuvre.getAuteur().getNom() <<", en "<<oeuvre.getLangue();}
};

class Bibliotheque
{
    private:   
        string nom ;
        vector<Exemplaire> liste ;
    public:
        Bibliotheque(const string nom) : nom(nom) {  cout << "La bibliothèque "<<nom<<" est ouverte !"<<endl;}
        string getNom()const {  return nom ;}

        void stocker(Oeuvre& o, int nombre=1) {
           
            for (int i=0; i< nombre ; i++) {  liste.push_back(Exemplaire(o)); }
        }

        void lister_exemplaires(string langue="") const 
        {
            for(int i = 0 ; i < liste.size() ; i ++){
            if(langue =="")
               { cout << "Un exemplaire de "<<liste[i].getOeuvre().getTitre()
                     <<", "<<liste[i].getOeuvre().getAuteur().getNom()
                     <<", en "<<liste[i].getOeuvre().getLangue()<<endl;}
            else if ( liste[i].getOeuvre().getLangue() == langue )
                { cout << "Un exemplaire de "<<liste[i].getOeuvre().getTitre()
                     <<", "<<liste[i].getOeuvre().getAuteur().getNom()
                     <<", en "<<liste[i].getOeuvre().getLangue()<<endl;}
                     }
        }
        int compter_exemplaires(Oeuvre& oeuvre) const 
        {   
            int x = 0;
            for(int i = 0 ; i < liste.size() ; i ++)
            if ( liste[i].getOeuvre().getAuteur().getNom() == oeuvre.getAuteur().getNom() && liste[i].getOeuvre().getTitre() == oeuvre.getTitre())
                ++x ;
            return x;
        }
        void afficher_auteurs(bool prix=false) 
        {
                for(int i = 0 ; i < liste.size(); i++)
                {
                    if(!prix || liste[i].getOeuvre().getAuteur().getPrix())
                    cout << liste[i].getOeuvre().getAuteur().getNom() <<endl;
                }
        }
        ~Bibliotheque() {cout << "La bibliothèque "<<nom<<" ferme ses portes,"<<endl<<"et détruit ses exemplaires : " << endl;}

};

/*******************************************
 * Ne rien modifier après cette ligne.
 *******************************************/

int main()
{
  Auteur a1("Victor Hugo"),
         a2("Alexandre Dumas"),
         a3("Raymond Queneau", true);

  Oeuvre o1("Les Misérables"           , a1, "français" ),
         o2("L'Homme qui rit"          , a1, "français" ),
         o3("Le Comte de Monte-Cristo" , a2, "français" ),
         o4("Zazie dans le métro"      , a3, "français" ),
         o5("The Count of Monte-Cristo", a2, "anglais" );

  Bibliotheque biblio("municipale");
  biblio.stocker(o1, 2);
  biblio.stocker(o2);
  biblio.stocker(o3, 3);
  biblio.stocker(o4);
  biblio.stocker(o5);

  cout << "La bibliothèque " << biblio.getNom()
       << " offre les exemplaires suivants :" << endl;
  biblio.lister_exemplaires();

  const string langue("anglais");
  cout << " Les exemplaires en "<< langue << " sont :" << endl;
  biblio.lister_exemplaires(langue);

  cout << " Les auteurs à succès sont :" << endl;
  biblio.afficher_auteurs(true);

  cout << " Il y a " << biblio.compter_exemplaires(o3) << " exemplaires de "
       << o3.getTitre() << endl;

  return 0;
}