#include <iostream>
#include <string>
using namespace std;

class Timbre
{
private:
  static constexpr unsigned int ANNEE_COURANTE = 2016;

  /*****************************************************
   * Compléter le code à partir d'ici
   *****************************************************/
protected:
  string       nom      ;
  unsigned int annee    ;
  string       pays     ;
  double valeur_faciale ;
public:
  Timbre(string n ,unsigned int a ,string p ="Suisse",double v=1.0):nom(n),annee(a),pays(p),valeur_faciale(v){}
  double vente()const { return (annee <= 5) ? valeur_faciale : (valeur_faciale * 2.5 * age()) ;}
  unsigned int age()const {return (ANNEE_COURANTE - annee) ;}
  ostream& afficher(ostream& sortie )const 
  {
    sortie<<"de nom "<<nom
    <<" datant de "<<annee<<" (provenance "
    <<pays<<") ayant pour valeur faciale "<<valeur_faciale<<" francs";
    return sortie ;
  }
};
ostream& operator<<(ostream& sortie , Timbre const& t) 
{sortie << "Timbre " ;
  return  t.afficher(sortie) ; }

class Rare : public Timbre
{
  private:
    unsigned int nbr ;
  public:
    double vente()const {
      int x ;
      if(nbr < 100) x = 600 ;
      else if(nbr < 1000) x = 400 ;
      else x = 50 ;
      return x*(age()/10.0) ; }
      Rare(string n,unsigned int a,string p="Suisse",double v=1.0,unsigned int nb=100):Timbre(n,a,p,v),nbr(nb){}   
      unsigned int nb_exemplaires()const { return nbr ;}
};
ostream& operator<<(ostream& sortie , Rare const& r) 
{sortie<<"Timbre rare ("<<r.nb_exemplaires()<<" ex.) ";
 return r.afficher(sortie) ; }

class Commemoratif : public Timbre
{
  public:
    double vente()const { return (Timbre::vente()*2) ; }
    Commemoratif(string n ,unsigned int a ,string p = "Suisse" , double v = 1.0):Timbre(n,a,p,v){}
};

ostream& operator<<(ostream& sortie , Commemoratif const& c) 
{  sortie<<"Timbre  commémoratif ";
   return c.afficher(sortie);}
/*******************************************
 * Ne rien modifier après cette ligne.
 *******************************************/
int main()
{
  /* Ordre des arguments :
  *  nom, année d'émission, pays, valeur faciale, nombre d'exemplaires
  */
  Rare t1( "Guarana-4574", 1960, "Mexique", 0.2, 98 );
  Rare t2( "Yoddle-201"  , 1916, "Suisse" , 0.8,  3 );

  /* Ordre des arguments :
  *  nom, année d'émission, pays, valeur faciale, nombre d'exemplaires
  */
  Commemoratif t3( "700eme-501"  , 2002, "Suisse", 1.5 );
  Timbre       t4( "Setchuan-302", 2004, "Chine" , 0.2 );

  /* Nous n'avons pas encore le polymorphisme :-(
   * (=> pas moyen de faire sans copie ici :-( )  */
  cout << t1 << endl;
  cout << "Prix vente : " << t1.vente() << " francs" << endl;
  cout << t2 << endl;
  cout << "Prix vente : " << t2.vente() << " francs" << endl;
  cout << t3 << endl;
  cout << "Prix vente : " << t3.vente() << " francs" << endl;
  cout << t4 << endl;
  cout << "Prix vente : " << t4.vente() << " francs" << endl;

  return 0;
}
