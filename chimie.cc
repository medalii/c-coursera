#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class Flacon
{
private:
  string nom;
  double volume;
  double pH;

public:
  /*****************************************************
    Compléter le code à partir d'ici
  *******************************************************/
#define BONUS

    Flacon(string nom , double volume , double ph):nom(nom) , volume(volume) , pH(ph){}
    ostream& etiquette(ostream& sortie) const ;
    string getNom()const {return nom ;}
    double getVolume()const {return volume ;}
    double getPH()const {return pH ;}
    const Flacon operator+=(Flacon const& f) ;
  
};

ostream& Flacon::etiquette(ostream& sortie) const 
{
  sortie<< nom << " : " << volume << " ml, pH " << pH ;
  return sortie ;
}

const Flacon operator+(Flacon const& f1 , Flacon const& f2)
{
  return Flacon(f1.getNom()+" + "+f2.getNom(),f1.getVolume()+f2.getVolume(),-log10((f1.getVolume()*pow(10.0,-f1.getPH())+f2.getVolume()*pow(10.0,-f2.getPH()))/(f1.getVolume()+f2.getVolume()))) ;
}
const Flacon Flacon::operator+=(Flacon const& f)
{
    nom +=(" + "+f.getNom());
    volume += f.getVolume() ;
    pH= -log10( (volume*pow(10.0,-pH) + f.getVolume()*pow(10.0,-f.getPH()) ) / (volume+f.getVolume()) ) ;
    return *this ;
}
ostream& operator<<(ostream& sortie,Flacon const& f) {   return f.etiquette(sortie) ;}
/*******************************************
 * Ne rien modifier après cette ligne.
 *******************************************/

void afficher_melange(Flacon const& f1, Flacon const& f2)
{
  cout << "Si je mélange " << endl;
  cout << "\t\"" << f1 << "\"" << endl;
  cout << "avec" << endl;
  cout << "\t\"" << f2 << "\"" << endl;
  cout << "j'obtiens :" << endl;
  cout << "\t\"" << (f1 + f2) << "\"" << endl;
}

int main()
{
  Flacon flacon1("Eau", 600.0, 7.0);
  Flacon flacon2("Acide chlorhydrique", 500.0, 2.0);
  Flacon flacon3("Acide perchlorique",  800.0, 1.5);

  afficher_melange(flacon1, flacon2);
  afficher_melange(flacon2, flacon3);

  return 0;

}
