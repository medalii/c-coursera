#include <iostream>
#include <string>
using namespace std ;
class Eluche {
    private:
        string nom ;
        string espece ;
        double prix ;

    public :
        string getNom()   const { return nom ;}
        string getEspece()const { return espece ;}
        double getPrix()  const { return prix ;}
        void   setPrix( double val){ prix = val ;}
        void   etiquette() const{ cout<<nom+"\t"+espece+"\t"<<prix << endl ;}
        Eluche(string n = "a" ,string  e= "b",double  p = 0):nom(n),espece(e),prix(p)
        {
            nom = n ; espece = e ; prix = p ;
        }

}  ; 
