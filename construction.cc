#include <iostream>
#include <string>
#include <vector>
using namespace std;

// Pour simplifier
typedef string Forme   ;
typedef string Couleur ;

class Brique
{
private:
  Forme   forme   ;
  Couleur couleur ;

public:
  /*****************************************************
    Compléter le code à partir d'ici
  *******************************************************/
    Brique() = delete;
    Brique(Forme f , Couleur c){  forme = f   ; couleur = c;}
    ostream& afficher(ostream& sortie) const ;
};
ostream& Brique::afficher(ostream& sortie) const
{
    return (couleur=="") ? sortie<<"("<<forme<<")" : sortie<<"("<<forme<<", "<< couleur <<")" ;
}
ostream& operator<<(ostream& sortie, Brique const& brique){  return brique.afficher(sortie) ;}

class Construction  
{
  friend class Grader;
  private:
    vector<vector<vector<Brique>>> contenu ;
  public:
    Construction(Brique& b);
    ostream& afficher(ostream& sortie) const ;
    Construction& operator^=(Construction const&);
    Construction& operator-=(Construction const&);
    Construction& operator+=(Construction const&);

};
Construction::Construction(Brique& b) {  contenu = {{{b}}}; }
ostream& operator<<(ostream& sortie , Construction c) {  return c.afficher(sortie) ; }
ostream& Construction::afficher(ostream& sortie)const
{
    if (contenu.size() == 0) return sortie ;
    for(int i = contenu.size() -1 ; i >= 0  ; i-- ){
      sortie<<"Couche "<< i <<" : "<<endl ;
      for(int j = contenu[i].size() -1 ; j >= 0 ; j-- ){
        for(int k = 0 ; k < contenu[i][j].size() ; k++ ){
          contenu[i][j][k].afficher(sortie) ;
        }
        sortie << endl ;}}
  return sortie ;}

Construction& Construction::operator^=(Construction const& a)
{
  for(int i = 0 ; i < a.contenu.size() ; i ++) contenu.push_back(a.contenu[i]);
  return *this ;
}
const Construction operator^(Construction a , Construction const& b)
{ a ^= b;
 return a; }

Construction& Construction::operator-=(Construction const& b) {
  if (b.contenu.size() >= contenu.size()) {
    for (int i = 0; i < contenu.size(); i++) {
      for (int j = 0; j < b.contenu[i].size(); j++) contenu[i].push_back(b.contenu[i][j]);
    }
  }
  return *this;
}

const Construction operator-(Construction a, Construction const& b) {
  a -= b;
  return a;
}

Construction& Construction::operator+=(Construction const& b) {
  if (b.contenu.size() < contenu.size()) return *this;
  if (b.contenu[0].size() < contenu[0].size()) return *this;
  for (int i = 0; i < contenu.size(); i++) {
    for (int j = 0; j < contenu[0].size(); j++) {
      for (int k = 0; k < b.contenu[i][j].size(); k++) {
        contenu[i][j].push_back(b.contenu[i][j][k]);
      }
    }
  }
  return *this;
}

const Construction operator+(Construction a , Construction const& b)
{ a += b;
 return a; }

const Construction operator*(unsigned int n, Construction const& a)
{  n--;
  Construction x = a;
  while (n--) x += a;
  return x;
}

const Construction operator/(unsigned int n, Construction const& a)
{  n--;
  Construction x = a;
  while (n--) x ^= a;
  return x;
}

const Construction operator%(unsigned int n, Construction const& a)
{  n--;
  Construction x = a;
  while (n--) x -= a;
  return x;
}

/*******************************************
 * Ne rien modifier après cette ligne.
 *******************************************/

int main()
{
  // Modèles de briques
  Brique toitD("obliqueD", "rouge");
  Brique toitG("obliqueG", "rouge");
  Brique toitM(" pleine ", "rouge");
  Brique mur  (" pleine ", "blanc");
  Brique vide ("                 ", "");

  unsigned int largeur(4);
  unsigned int profondeur(3);
  unsigned int hauteur(3); // sans le toit

  // on construit les murs
  Construction maison( hauteur / ( profondeur % (largeur * mur) ) );

  // on construit le toit
  Construction toit(profondeur % ( toitG + 2*toitM + toitD ));
  toit ^= profondeur % (vide + toitG + toitD);

  // on pose le toit sur les murs
  maison ^= toit;

  // on admire notre construction
  cout << maison << endl;

  return 0;
}
