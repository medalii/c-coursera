#include <iostream>
using namespace std;

/*****************************************************
 * Compléter le code à partir d'ici
 *****************************************************/
class Patient{
    private :
    double masse ;
    double hauteur  ;
    public :
    double poids()const { return masse ;}
    double taille()const{ return hauteur;}
    void init(double m ,double h){
        if(m > 0 && h > 0){
        masse = m ;     hauteur = h ;  }
        else {
            masse = 0 ; hauteur = 0 ;
        }
    }
    void afficher()const{
        cout<<"Patient : "<<masse<<" kg pour "<< hauteur<<" m"<<endl ;
    }
    double imc()const{
        return  ( hauteur > 0 )? masse/(hauteur*hauteur) : 0 ;
    }
};

/*******************************************
 * Ne rien modifier après cette ligne.
 *******************************************/

int main()
{
  Patient quidam;
  double poids, taille;
  do {
    cout << "Entrez un poids (kg) et une taille (m) : ";
    cin >> poids >> taille;
    quidam.init(poids, taille);
    quidam.afficher();
    cout << "IMC : " << quidam.imc() << endl;
  } while (poids * taille != 0.0);
  return 0;
}
