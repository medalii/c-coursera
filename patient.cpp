/*******************************************
* Complétez le programme à partir d’ici.
*******************************************/
#include <string>
#include <iostream>
using namespace std ;
class patient{
    private :
    double masse ;
    double hauteur  ;
    public :
    void init(double m ,double h){
        if(m > 0 && h > 0){
        masse = m ;     hauteur = h ;  }
        else {
            masse = 0 ; hauteur = 0 ;
        }
    }
    void affiche()const{
        cout<<"Patient : "<<masse<<" kg pour "<< hauteur<<" m"<<endl ;
    }
    void imc()const{
        double icm = ( hauteur > 0 )? masse/(hauteur*hauteur) : 0 ;
        cout <<"ICM : " << icm <<endl ;
    }
};

int main()
{
    patient p ;
    double a,b ;
    cout << "Entrez un poids (kg) et une taille (m) :";
    cin >> a >> b;
    p.init(a,b);
    p.affiche();
    p.imc();
    return 0;
}


/*******************************************
* Ne rien modifier après cette ligne.
*******************************************/
